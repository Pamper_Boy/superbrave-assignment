
import { products } from './data'
import {createDosageItem, createQuantityItem, createsummary} from './create'

let dropdown = $('#products');
var dosageIndex = null;
var selectedProductIndex
var product, dosage, quantity, price

dropdown.empty();

dropdown.append('<option selected="true" disabled>Kies een product</option>');
dropdown.prop('selectedIndex', 0);

$.each(products, function (index, entry) {
  dropdown.append($('<option></option>')
    .attr('value', entry.name)
    .attr('data-index', index)
    .text(entry.name));
})

dropdown.on('change', function(){
  $("#product-selector").addClass("input-validate--aproved")
  $("#dosage").empty()
  $("#quantity").empty()
  selectedProductIndex = dropdown[0].selectedIndex - 1;
  product = products[selectedProductIndex].name
  renderItems( products[selectedProductIndex].dosages, createDosageItem )
});

function renderItems(array, functionName) {
  $.each(array, function (index, entry) {
    functionName(index, entry)
  })
}

export function selectRadio() {
  var parent = $("#"+this.id);
  var input = parent.find('input')
  input.prop("checked", true);

  if(input.attr("name") === "dosage") {
    dosage = parent.find('span').html();
    dosageIndex = parent.find('input').attr('index')
    renderQuantity()
  } else if(input.attr("name") === "quantity") {
    quantity = parent.children().eq(1).html()
    price = parent.children().eq(2).html()
    dosageIndex = parent.find('input').attr('index')
  }

  appendCheck(parent)
  renderText()
}

function appendCheck(parent) {
  var input = $('input')
  var groupName = $("#"+parent.parent().attr('id'))

  if(input.not(':checked')) {
    groupName.find("input")
    .parent()
    .removeClass("input-validate--aproved-cb")
  }
  if(input.is(':checked')) {
    parent.addClass("input-validate--aproved-cb")
  }
}

function renderQuantity() {
  $("#quantity").empty()

  if(dosageIndex !== null) {
    renderItems( products[selectedProductIndex]
      .dosages[dosageIndex].packages, createQuantityItem )
  }
}

function renderText() {
  if(product && dosage && quantity && price) {
    createsummary(product, dosage, quantity, price)
  }
}

renderText()
