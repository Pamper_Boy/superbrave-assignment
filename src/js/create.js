import { selectRadio } from './script'

export function createDosageItem(index, data) {
  var wrapperId = "dosage-wrap-" + nextItem("dosage");
  var radioId = "dosage-" + nextItem("dosage");

  var dosageItem = $("<span></span>");
  dosageItem.addClass("col-2 input-checkbox");
  dosageItem.attr('id', wrapperId);
  dosageItem.bind('click', selectRadio);

  var radio = $('<input>');
  radio.attr('type', 'radio');
  radio.attr('id', radioId);
  radio.attr('name', "dosage");
  radio.attr('index', index);

  var textDosage = $("<span></span>");
  textDosage.addClass("text center");
  textDosage.html(data.dosage);

  dosageItem.append(radio);
  dosageItem.append(textDosage);

  $("#dosage").append( dosageItem );
}

export function createQuantityItem(index, data) {
  var wrapperId = "quantity-wrap-" + nextItem("quantity");
  var radioId = "quantity-" + nextItem("quantity");

  var quantityItem = $("<span></span>");
  quantityItem.addClass("col-3 input-checkbox");
  quantityItem.attr('id', wrapperId);
  quantityItem.bind('click', selectRadio);


  var radio = $('<input>');
  radio.attr('type', 'radio');
  radio.attr('id', radioId);
  radio.attr('name', "quantity");

  var textQnty = $("<span></span>");
  textQnty.addClass("text center");
  textQnty.html(data.amount + " " + data.type);

  var textPrice = $("<span></span>");
  textPrice.addClass("text center font-heavy");
  textPrice.html("€ " + data.price + ",00");

  quantityItem.append(radio);
  quantityItem.append(textQnty);
  quantityItem.append(textPrice);

  $("#quantity").append( quantityItem );
}

export function createsummary(product, dosage, quantity, price) {
  $("#summary").empty()
  var summary = $("<p></p>");
  summary.addClass("pricing");

  var span = $("<span></span>");
  span.addClass("font-heavy");
  span.html("U heeft gekozen voor:")

  var br = $("<br>");

  var productText = $("<span id='productText'>" + product +
  "</span> met een dosering van <span id='dosageText'>" + dosage +
  "</span> en <span id='quantityText'>" + quantity +
  "</span> (incl. consult en service fee)<br> De kosten van deze opdracht bedraagt <span id='priceText' class='font-heavy'>" + price + "</span>.");

  summary.append(span);
  summary.append(br);
  summary.append(productText);

  $("#summary").append( summary );
}

function nextItem(name) {
  return $("input[name='" + name + "']" ).length + 1;
}
